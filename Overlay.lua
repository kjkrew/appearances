--[[
	Appearances Overlay Functions
	Version <%version%>

	Revision: $Id: Overlay.lua 1 2017-06-06 18:38:42 Pacific Daylight Time Kjasi $
]]

local ap = _G.Appearances
if (not ap) then
	print(RED_FONT_COLOR_CODE.."Main is unable to find Appearances Global."..FONT_COLOR_CODE_CLOSE)
	return
end

ap.Overlay = {}
local OL = ap.Overlay

function OL:AddIconOverlayToItem(bagslotID)
	-- Spawn an Appearances_Overlay_Template onto the item
	
end

function OL:SetOverlayTo(OverlaySetting)
	
end

function OL:AddAndSetIconOverlay(bagslotID, OverlaySetting)
	OL:AddIconOverlayToItem(bagslotID)
	OL:SetOverlayTo(OverlaySetting)
end