--[[
	Appearances
	English Localization File
	Version <%version%>
	
	Revision: $Id: enUS.lua 1 2016-09-17 20:06:22 Pacific Daylight Time Kjasi $
	
	This file affects non-critical (graphic) localizations only.
]]

local ap = _G.Appearances
if (not ap) then
	print(RED_FONT_COLOR_CODE.."Unable to find Appearances Global.|r")
	return
end
local L = ap.Localization
if (not L) then
	print(RED_FONT_COLOR_CODE.."Unable to find Appearances Localization.|r")
	return
end

L["Char List Levels"] = "%s (%i/%i)"			-- Character name, current Level, Required Level
L["List Separator"] = "%s, %s"		-- For lists that look like: Mage, Priest, or Warlock
L["List New Line Separator"] = "%s,\n%s"		-- For lists that look like: Mage, Priest, or Warlock
L["List Finale"] = "%s, or %s"			-- For the end of lists that look like: Mage, Priest, or Warlock

L["Learnable By"] = "Appearance learnable by:"							-- Followed by a list of class on the current realm and faction.
L["Can eventually Learn"] = "Appearance will be learnable by:"	-- Followed by a list of class on the current realm and faction.
L["Error Class List"] = "Unable to get Class List"