## Interface: 80200
## Title: Appearances
## Author: Kjasi
## Version: <%version%>
## Notes: Helps determine which appearances you have or have not collected
## URL: https://bitbucket.org/Kjasi/kjasis-wow-addons
## X-URL: https://bitbucket.org/Kjasi/kjasis-wow-addons
## X-Feedback: https://bitbucket.org/Kjasi/kjasis-wow-addons
## DefaultState: Enabled
## LoadOnDemand: 0
##
#  myAddOns Support Metadata
## X-Website: https://bitbucket.org/Kjasi/kjasis-wow-addons
## X-Category: Information
##
## SavedVariables: Appearances_Collection
Load.xml