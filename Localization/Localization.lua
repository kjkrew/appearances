﻿--[[
	Appearances
	Primary Localization File
	Version <%version%>
	
	Revision: $Id: Localization.lua 1 2016-09-16 14:03:34 Pacific Daylight Time Kjasi $
	
	This file affects critical functions of Appearances. Without these Localizations, Appearances will not work properly.
	
	-- NOTE: Some items in this file (such as the commandline options) were translated by Google. Please check them to see if they are correct!
]]

local ap = _G.Appearances
if (not ap) then
	print(RED_FONT_COLOR_CODE.."Unable to find Appearances Global.|r")
	return
end
ap.Localization = {}
local L = ap.Localization

-- [[ enUS & enGB ]] --
L["AddonName"] = "Appearances"

L["Message Normal"] = "%s:|r %s"							-- Appearances title, then Message = "Appearances: Message"
L["Message Error"] = "%s Error:|r %s"						-- Appearances title, then Message = "Appearances Error: Error Message"
L["Message Debug"] = "%s Debug Message:|r %s"		-- Appearances title, then Message = "Appearances Debug Message: Debug Message"

-- [[ deDE ]] --
if (GetLocale() == "deDE") then
	L["Message Normal"] = "%s:|r %s"
	L["Message Error"] = "%s Fehler:|r %s"
	L["Message Debug"] = "%s Debugmeldung:|r %s"

	-- [[ frFR ]] --
elseif (GetLocale() == "frFR") then
	L["Message Normal"] = "%s:|r %s"
	L["Message Error"] = "Erreur %s:|r %s"
	L["Message Debug"] = "Déboguer %s:|r %s"
	
	-- [[ esES & esMX ]] --
elseif (GetLocale() == "esES") or (GetLocale() == "esMX") then
	L["Message Normal"] = "%s:|r %s"
	L["Message Error"] = "Error %s:|r %s"
	L["Message Debug"] = "Mensaje de depuración %s:|r %s"

	-- [[ itIT ]] --
elseif (GetLocale() == "itIT") then
	L["Message Normal"] = "%s:|r %s"
	L["Message Error"] = "Errore  %s:|r %s"
	L["Message Debug"] = "di debug %s:|r %s"
	
	-- [[ ptBR & ptPT ]] --
elseif (GetLocale() == "ptBR") or (GetLocale() == "ptPT") then
	L["Message Normal"] = "%s:|r %s"
	L["Message Error"] = "Erro %s:|r %s"
	L["Message Debug"] = "Debug %s:|r %s"
	
	-- [[ ruRU ]] --
elseif (GetLocale() == "ruRU") then
	L["Message Normal"] = "%s:|r %s"
	L["Message Error"] = "%s ошибке:|r %s"
	L["Message Debug"] = "%s отладки:|r %s"
	
	-- [[ koKR ]] --
elseif (GetLocale() == "koKR") then
	L["Message Normal"] = "%s:|r %s"
	L["Message Error"] = "%s 오류:|r %s"
	L["Message Debug"] = "%s 디버그:|r %s"
	
	-- [[ zhCN ]] --
elseif (GetLocale() == "zhCN") then
	L["Message Normal"] = "%s:|r %s"
	L["Message Error"] = "%s错误:|r %s"
	L["Message Debug"] = "%s调试:|r %s"
	
	-- [[ zhTW ]] --
elseif (GetLocale() == "zhTW") then
	L["Message Normal"] = "%s:|r %s"
	L["Message Error"] = "%s錯誤:|r %s"
	L["Message Debug"] = "%s調試:|r %s"

end