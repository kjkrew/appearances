--[[
	Appearances Main Functions
	Version <%version%>

	Revision: $Id: Main.lua 4 2019-09-06 13:01:23 Pacific Time Kjasi $
]]

local ap = _G.Appearances
if (not ap) then
	print(RED_FONT_COLOR_CODE.."Main is unable to find Appearances Global."..FONT_COLOR_CODE_CLOSE)
	return
end

local L = ap.Localization
if (not L) then
	print(RED_FONT_COLOR_CODE.."Main is unable to find Appearance's Localization data."..FONT_COLOR_CODE_CLOSE)
	return
end
local P = ap.PlayerData
if (not P) then
	print(RED_FONT_COLOR_CODE.."Main is unable to find Appearance's Player data."..FONT_COLOR_CODE_CLOSE)
	return
end
--local OL = ap.Overlay
--if (not OL) then
--	print(RED_FONT_COLOR_CODE.."Main is unable to find Appearance's Overlay functions."..FONT_COLOR_CODE_CLOSE)
--	return
--end

-- Libraries
local LibKJ = LibStub("LibKjasi-1.0")
-- Setup our addon with KjasiLib
LibKJ:setupAddon({
	["Name"] = L["AddonName"],
	["Version"] = ap.Version,
	["MsgStrings"] = {
		["Default"] = GREEN_FONT_COLOR_CODE..L["Message Normal"],
		["error"] = RED_FONT_COLOR_CODE..L["Message Error"],
		["debug"] = LIGHTYELLOW_FONT_COLOR_CODE..L["Message Debug"],
	},
	["ChannelLevel"] = ap.debug,
})

function ap:Msg(msg,channel)
	if (msg == nil) then return end
	LibKJ:Msg(L.AddonName, msg, channel)
end

function Appearances_Load(self)
	self:RegisterEvent("VARIABLES_LOADED")
	self:RegisterEvent("PLAYER_LEVEL_UP")
	self:RegisterEvent("TRANSMOG_COLLECTION_UPDATED");

	SlashCmdList["Appearances"] = Appearances_CommandLine
	SLASH_Appearances1 = "/appearances"
	SLASH_Appearances2 = "/ap"
end

function Appearances_OnEvent(self, event, ...)
	local arg1, _ = ...
	if (event =="VARIABLES_LOADED") then
		ap.Loaded()
	elseif (event == "TRANSMOG_COLLECTION_UPDATED") then
		ap:GatherAppearances()
	elseif (event == "PLAYER_LEVEL_UP") then
		Appearances_Collection["Characters"][P.Class][ap:GetDBPlayerName()].Level = arg1
	end
end


----------------------
-- Helper Functions --
----------------------
function ap:GetItemID(link)
	return tonumber(link:match("item:(%d+)"))
end
function ap:GetItemLink(id)
	return select(2, GetItemInfo(id))
end
function ap:GetSubclass(link)
	return select(7, GetItemInfo(link))
end
function ap:SubclassesMatch(link1, link2)
	local sbc1 = ap:GetSubclass(link1)
	local sbc2 = ap:GetSubclass(link2)
	if sbc1 == nil or sbc2 == nil then return end
	return sbc1 == sbc2
end
function ap:GetAllAppearances()
	C_TransmogCollection.ClearSearch(LE_TRANSMOG_SEARCH_TYPE_ITEMS)
	local applist = {}
	for catid=1,28 do
		CatApps = C_TransmogCollection.GetCategoryAppearances(catid)
		for i, CatApp in pairs(CatApps) do
			if CatApp.isCollected then
				applist[CatApp.visualID] = CatApp
			end
		end
	end
	return applist
end
function ap:GetSourceID(itemLink)
	local iid, _, _, slotname = GetItemInfoInstant(itemLink)
	local slotlist = ap.InventorySlotMap[slotname]
	
	-- Die if we can't check it out.
	if slotlist == nil or IsDressableItem(itemLink) == false then return end
	ap.DressUpModel:SetUnit("player")
	ap.DressUpModel:Undress()
	for i, slot  in pairs(slotlist) do
		ap.DressUpModel:TryOn(itemLink, slot)
		local srcID = ap.DressUpModel:GetSlotTransmogSources(slot)
		if srcID ~= 0 then
			return srcID
		end
	end
end
function ap:GetAppearanceID(link)
	local srcID = ap:GetSourceID(link)
	if srcID ~= nil then
		local appID = select(2, C_TransmogCollection.GetAppearanceSourceInfo(srcID))
		--print(C_TransmogCollection.GetAppearanceSourceInfo(srcID))
		return appID
	end
end
function ap:PlayerHasAppearance(link)
	local appID = ap:GetAppearanceID(link)
	if appID == nil then return false end
	if Appearances_Collection["Appearances"][appID] and Appearances_Collection["Appearances"][appID].isCollected then
		return true
	end
	local applist = ap:GetAllAppearances()
	if applist[appID] and applist[appID].isCollected then
		Appearances_Collection["Appearances"][appID] = applist[appID]
		return true
	end
	return false
end

function ap:GetDBPlayerName()
	if not P then return end
	return P.Name.."-"..P.Realm
end

function ap:IsValidItem(itemLink)
	local itemSlot = select(9, GetItemInfo(itemLink))
	if IsDressableItem(itemLink) == true and ap.InventorySlotMap[itemSlot] ~= nil then
		return true
	end
	return
end

function ap:ClassCanUse(itemLink, class)
	if not class then class = P.Class end
	local classLocal = GetClassInfo(ap.ClassIDMap[class])
	local itemName = GetItemInfo(itemLink)
	local itemQuality = select(3, GetItemInfo(itemLink))
	local minLevel = select(5, GetItemInfo(itemLink))
	local itemSlotID = select(9, GetItemInfo(itemLink))
	local itemClassID = select(12, GetItemInfo(itemLink))
	local itemSubClassID = select(13, GetItemInfo(itemLink))
	local classOnlyScan = gsub(ITEM_CLASSES_ALLOWED,"%%s","(.+)")
	local LevelScan = gsub(ITEM_MIN_LEVEL,"%%d","(.+)")
	--print(format("Item: %s, TypeID: %s, SubID: %s", itemName, itemClassID, itemSubClassID))
	
	-- Only Greens and Above
	if (itemQuality < 2) then
		return
	end
		
	-- Find Class-Only items
	if (ap:TooltipFind(classOnlyScan)) then
		--print("Class Limit Found")
		local classes = select(2,ap:TooltipFind(classOnlyScan))
		if not strfind(classes[1],classLocal) then
			return
		end
		--print("Classes Match: "..classes[1].. " VS "..classLocal)
	end
	
	-- Skip soulbound items
	if (ap:TooltipFind(ITEM_BIND_ON_PICKUP)) then
		return
	end
	
	-- Skip Non-Binding items
	if (not ap:TooltipFind(ITEM_BIND_ON_EQUIP))and(not ap:TooltipFind(ITEM_BIND_ON_USE)) then
		return
	end
	
	 -- Armor
	if itemClassID == 4 then
		-- Cloaks/Back pieces, usable by all
		if itemSlotID == "INVTYPE_CLOAK" then
			return true, minLevel
		end
		-- Cosmetic, usable by all
		if itemSubClassID == 5 then
			return true, minLevel
		end
		-- Shields
		if itemSubClassID == 6  and (class == "PALADIN" or class == "WARRIOR" or class == "SHAMAN") then
			return true, minLevel
		-- Other Armor
		elseif itemSubClassID == ap.ClassArmorTypes[class] then
			return true, minLevel
		end

	-- Weapons	
	elseif itemClassID == 2 then
		if tContains(ap.ClassWeaponTypes[class], itemSubClassID) then
			return true, minLevel
		end
	end
end

function ap:HasClassCanUse(itemLink)
	local canUse = false
	local classList = {}
	local classListFuture = {}
	
	-- Set tooltip for Scanning
	Appearances_Tooltip_Scanner:ClearLines()
	Appearances_Tooltip_Scanner:SetHyperlink(itemLink)
	--local soulbound, list = ap:TooltipFind(ITEM_BIND_ON_PICKUP)
	--print("SB: "..tostring(soulbound))
	
	-- Check to see if this class can use this item.
	for class, CharList in pairs(Appearances_Collection["Characters"]) do
		local classUse, minLevel = ap:ClassCanUse(itemLink, class)
		--print(format("For class: %s, CanUse: %s, MinLevel: %s", class, tostring(classUse), tostring(minLevel)))
		local thisRealm = false
		local maxLevel = 1
		for CharID,data in pairs(CharList) do
			if classUse and classUse == true and data.Realm == P.Realm and data.Faction == P.Faction then
				thisRealm = true
				data.itemLevel = minLevel
				if data.Level < minLevel then
					tinsert(classListFuture, data)
				else
					tinsert(classList, data)
				end
			end
		end
		--print(format("Class %s canUse: %s", class, classUse?"true":"false"))
		if classUse == true and thisRealm == true then canUse = true end
	end
	
	--print("CCU Returns: CanUse: "..tostring(classCanUse)..", #ClassList: "..tostring(#classList)..", #ClassListFuture: "..tostring(#classListFuture))
	
	return canUse, classList, classListFuture
end

function Appearances_LoadBlizzCollections()
	local BCol = UIParentLoadAddOn("Blizzard_Collections")
	ap:GatherAppearances()
end

-----------------------
-- Primary Functions --
-----------------------
function ap:Loaded()
	ap:buildDatabase()
	C_TransmogCollection.SetShowMissingSourceInItemTooltips(false)
	LibKJ:Timer(L["AddonName"], "LoadCollectionsTimer", 2, "Appearances_LoadBlizzCollections")
	ap:Msg("Loaded")
end

function Appearances_CommandLine(cmd)
	print("No commands at this time")
end

function ap:buildDBGlobal()
	if not Appearances_Collection then
		Appearances_Collection = {}
	end
end

function ap:buildDBChar()
	ap:buildDBGlobal()
	if not P then return end
	class = P.Class
	prname = ap:GetDBPlayerName()

	if not Appearances_Collection["Characters"] then
		Appearances_Collection["Characters"] = {}
	end
	if not Appearances_Collection["Characters"][class] then
		Appearances_Collection["Characters"][class] = {}
	end
	if not Appearances_Collection["Characters"][class][prname] then
		Appearances_Collection["Characters"][class][prname] = P
	end
	Appearances_Collection["Characters"][class][prname].Level = UnitLevel("player")
end

function ap:GatherAppearances()
	ap:buildDBAppearances()
	
	local AppearanceList = ap:GetAllAppearances()
	for k,v in pairs(AppearanceList) do
		if not Appearances_Collection["Appearances"][k] then
			Appearances_Collection["Appearances"][k] = v
		end
	end
end

function ap:buildDBAppearances()
	ap:buildDBGlobal()
	if not Appearances_Collection["Appearances"] then
		Appearances_Collection["Appearances"] = {}
	end
end

function ap:buildDatabase()
	-- Global setup
	ap:buildDBGlobal()
	
	-- Character Setups
	ap:buildDBChar()
	
	-- Appearances
	ap:buildDBAppearances()
end

--------------
-- Tooltips --
--------------
local function addTooltipCharList(tooltip, list, showLevels)
	if not showLevels then showLevels = false end
	local count = 0
	local classStr = ""
	for k,data in pairs(list) do
		local localname = data.Name
		if showLevels then
			localname = format(L["Char List Levels"],localname,data.Level,data.itemLevel)
		end
		local classColor = "|c"..RAID_CLASS_COLORS[data.Class].colorStr
		if count == 0 then
			classStr = classColor..localname..FONT_COLOR_CODE_CLOSE
		elseif (count+1) == #list then
			classStr = format(NORMAL_FONT_COLOR_CODE..L["List Finale"], classStr, classColor..localname..FONT_COLOR_CODE_CLOSE)
		else
			local sepstr = L["List Separator"]
			if count == 5 then sepstr = L["List New Line Separator"] end
			classStr = format(NORMAL_FONT_COLOR_CODE..sepstr, classStr, classColor..localname..FONT_COLOR_CODE_CLOSE)
		end
		count = count + 1
	end
	if count > 0 then
		tooltip:AddLine(classStr..FONT_COLOR_CODE_CLOSE)
	else
		tooltip:AddLine(L["Error Class List"])
	end
end

function ap:AddTooltipInfo(tooltip, itemLink)
	local itemInfo = GetItemInfo(itemLink)
    if itemInfo == nil then
        return
    end
	if ap:IsValidItem(itemLink) and ap:PlayerHasAppearance(itemLink) == false then
		local classCanUse, classList, classListFuture = ap:HasClassCanUse(itemLink)
		--print("classCanUse: "..tostring(classCanUse)..", #ClassList: "..tostring(#classList)..", #ClassListFuture: "..tostring(#classListFuture))
		if ap:ClassCanUse(itemLink) then
			-- tooltip:AddLine(BATTLENET_FONT_COLOR_CODE..TRANSMOGRIFY_STYLE_UNCOLLECTED..FONT_COLOR_CODE_CLOSE)
		elseif classCanUse then
			if #classList > 0 then
				-- List of Classes that can learn this
				tooltip:AddLine(GREEN_FONT_COLOR_CODE..L["Learnable By"]..FONT_COLOR_CODE_CLOSE)
				addTooltipCharList(tooltip, classList)
			end
			if #classListFuture > 0 then
				-- List of Classes that can eventually learn this
				tooltip:AddLine(YELLOW_FONT_COLOR_CODE..L["Can eventually Learn"]..FONT_COLOR_CODE_CLOSE)
				addTooltipCharList(tooltip, classListFuture, true)
			end
		end
	end
end

-- Tooltip Searching
function ap:TooltipFind(target)
	--print("Looking through Tooltip... Num Lines: "..Appearances_Tooltip_Scanner:NumLines())
	for i=1,Appearances_Tooltip_Scanner:NumLines() do
		local mytextL = _G[Appearances_Tooltip_Scanner:GetName().."TextLeft"..i]
		local mytextR = _G[Appearances_Tooltip_Scanner:GetName().."TextRight"..i]
		local textL = mytextL:GetText()
		local textR = mytextR:GetText()
		--print(format("Target: %s, TextL: %s, TextR: %s", target, tostring(textL), tostring(textR)))
		if (textL) and (strfind(textL,target)) then
			local s = {strmatch(textL,target)}
			return true, s
		end
		if (textR) and (strfind(textR,target)) then
			local s = {strmatch(textL,target)}
			return true, s
		end
	end
	return
end

function Appearances_SetItemTooltip(self)
    ap.tooltip = self
    local link = select(2, self:GetItem())
    if link then
        ap:AddTooltipInfo(ap.tooltip, link)
    end
end

function Appearances_SetHyperlinkTooltip(self, link)
    ap.tooltip = self
    local type, id = string.match(link, "^(%a+):(%d+)")
    if not type or not id then return end
    if type == "item" then
        ap:AddTooltipInfo(ap.tooltip, link)
    end
end


----------------------
-- Bags             --
----------------------

function ap:ScanBags()
	for bagID=0,4 do
		local slots = GetContainerNumSlots(bagID)
		for bagSlot=0,slots do
			local link = GetContainerItemLink(bagID, bagSlot)
			if ap:IsValidItem(link) == false then
				next()
			end
			
			print(link+"is valid")
			
			
		end
	end
end

------------
-- Hooks --
------------
GameTooltip:HookScript("OnTooltipSetItem", Appearances_SetItemTooltip)
ItemRefTooltip:HookScript("OnTooltipSetItem", Appearances_SetItemTooltip)
ItemRefShoppingTooltip1:HookScript("OnTooltipSetItem", Appearances_SetItemTooltip)
ItemRefShoppingTooltip2:HookScript("OnTooltipSetItem", Appearances_SetItemTooltip)
ShoppingTooltip1:HookScript("OnTooltipSetItem", Appearances_SetItemTooltip)
ShoppingTooltip2:HookScript("OnTooltipSetItem", Appearances_SetItemTooltip)
hooksecurefunc(GameTooltip, "SetHyperlink", Appearances_SetHyperlinkTooltip)